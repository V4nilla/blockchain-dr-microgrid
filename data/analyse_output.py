__author__ = 'Olivier Van Cutsem'

# ---- LOAD DATA ---- #

import pandas as pd
import numpy as np
import json
import math

# Global hardcoded data

coeff_grid_export = 0.75
coeff_local_sell = 1
time_step = 900.0/3600.0

def get_clean_output(folder_data, filename):
    """
    Clean SB and RES data

    :param folder_data:
    :param filename:
    :return:
    """

    # Data output
    df_data_raw = pd.read_csv(folder_data+filename)

    # Remove test signal
    df_data_raw = df_data_raw[df_data_raw["SG_ENTITY_ID"] >= 0]

    time_step = 0.25
    max_sb_id = 999
    actors_ids = set(df_data_raw["SG_ENTITY_ID"])

    if len(actors_ids) == 0:
        return None, None


    sb_ids = [v for v in actors_ids if v <= max_sb_id]
    res_ids = [v for v in actors_ids if v > max_sb_id]

    # df_data without RES: reset the index + extract RES
    df_data = df_data_raw[df_data_raw["SG_ENTITY_ID"] <= max_sb_id].reset_index()

    # RES data: make it negative
    df_data_res = df_data_raw[df_data_raw["SG_ENTITY_ID"] > max_sb_id]

    nb_cycles = len(df_data["SG_ENTITY_ID"])/len(sb_ids)  # remove the last cycle

    df_data["CYCLE"] = df_data.index / len(sb_ids)

    def str_to_float_list(current_row):
        if type(current_row["FORECAST_DATA"]) is not float:
            return [float(x) for x in current_row["FORECAST_DATA"]]

        return current_row["FORECAST_DATA"]  # This is a NaN

    def inverse_data(row):
        if type(row["FORECAST_DATA"]) is list:
            return [-x for x in row["FORECAST_DATA"]]

        return -row["FORECAST_DATA"]

    def clean_dayahea_algo(df):
        """
        1) Remove isolated rounds
        2) Replace NaN by the previous forecast
        3) Replace STR by vectors of float
        """

        # 1)
        last_cycle = max(df["CYCLE"])
        if len(df[df["CYCLE"] == last_cycle]) < len(sb_ids):
            df = df[df["CYCLE"] < max(df["CYCLE"])]

        # STR to list
        df['FORECAST_DATA'] = df['FORECAST_DATA'].str.split(',')
        df['FORECAST_DATA'] = df[['FORECAST_DATA']].apply(str_to_float_list, axis=1)

        # NaN -> previous values
        for i in sb_ids:
            df[df['SG_ENTITY_ID'] == i] = df[df['SG_ENTITY_ID'] == i].fillna(method='ffill')

        return df

    # Remove last cycle and transform STR to list of float
    df_data = clean_dayahea_algo(df_data)

    # Clean RES data
    df_data_res['FORECAST_DATA'] = df_data_res['FORECAST_DATA'].str.split(',')
    df_data_res['FORECAST_DATA'] = df_data_res[['FORECAST_DATA']].apply(str_to_float_list, axis=1)
    df_data_res['FORECAST_DATA'] = df_data_res[['FORECAST_DATA']].apply(inverse_data, axis=1)

    return df_data, df_data_res

# --- PRICE SIGNAL --- #
def get_resample_price(folder_data, filename):

    def resample(price_signal, time_step):
        ratio_time = int((24*3600/time_step) / len(price_signal))
        interpolated_signal = np.repeat(price_signal, ratio_time)
        return interpolated_signal

    price_data = json.load(open(folder_data+filename))["electricity_price"]
    for k, v in price_data.items():
        price_data[k] = resample(v, 15*60)
        price_data[k] = [p for p in price_data[k]]  # kWh -> Wh

    return price_data

# --- Useful functions ---
def compute_net_grid_power(forecast_data_comm):
    """
    Compute the net grid power of a community, from a list of forecast
    """
    res = None
    for v in forecast_data_comm.values:
        if res is None:
            res = v
        else:
            res = [x + y for x, y in zip(res, v)]

    return res


def compute_community_power(forecast_data_comm):
    """
    Compute local production from the community
    """

    res_prod = None
    res_cons = None
    for v in forecast_data_comm.values:

        # Production
        if res_prod is None:
            res_prod = [min(x, 0) for x in v]
        else:
            res_prod = [x + min(y, 0) for x, y in zip(res_prod, v)]

        # Consumption
        if res_cons is None:
            res_cons = [max(x, 0) for x in v]
        else:
            res_cons = [x + max(y, 0) for x, y in zip(res_cons, v)]

    return res_prod, res_cons


# --- COST FUNCTION definition --- #

def type_is_sb(id_actor):
    return id_actor < 1000


def cost_function(comm_data, price_data, verbose=True):
    """
    comm_data is a Pandas Dataframe containing the data of all the actors for 1 snapshot of the algo
    """
    map_cost = {}

    # Price rate
    import_lin_rate = price_data["energy_price"]
    import_quad_rate = price_data["quad_price"]
    export_rate = [coeff_grid_export * p for p in price_data["local_price"]]
    local_rate = price_data["local_price"]

    # ---- TOTAL COMMUNITY COST

    # 1) COST OF GRID IMPORT
    net_grid_flow = compute_net_grid_power(comm_data["FORECAST_DATA"])  # the grid import/export

    cost_import = sum([time_step * c_l * max(0, x_grid / 1000.0) for c_l, x_grid in zip(import_lin_rate, net_grid_flow)])

    quad_part = sum([time_step * c_q * max(0, x_grid / 1000.0) * max(0, x_grid / 1000.0) for c_q, x_grid in zip(import_quad_rate, net_grid_flow)])

    if verbose:
        print("LIN = {} | QUAD = {}".format(cost_import, quad_part))

    cost_import += quad_part

    # 2) COST OF LOCAL AND RES
    tot_local_prod, tot_local_cons = compute_community_power(comm_data["FORECAST_DATA"])  # the community production
    cost_local_prod = sum([- time_step * c_l * x_neg / 1000.0 for c_l, x_neg in zip(local_rate, tot_local_prod)])

    # 3) BENEFICE OF SELLING
    benef_export = sum([- time_step * c_l * min(0, x_grid / 1000.0) for c_l, x_grid in zip(export_rate, net_grid_flow)])

    community_cost_function = cost_import + cost_local_prod - benef_export
    print("Community cost function: {}".format(community_cost_function))

    # ---- PRO-RATED DISTRIBUTION
    daily_cons = sum(tot_local_cons)

    for i, data_actor in comm_data.iterrows():
        cost_cons = 0

        if daily_cons > 0:
            cost_cons = community_cost_function * sum([max(x_i, 0) for x_i in data_actor["FORECAST_DATA"]]) / daily_cons

        gain_prod = sum([-c_l * min(x_i/ 1000.0, 0) for c_l,x_i in zip(local_rate, data_actor["FORECAST_DATA"])])

        map_cost[data_actor["SG_ENTITY_ID"]] = (round(cost_cons, 3), round(gain_prod, 2))

    return map_cost


# --- COST per round --- #
def analyse_algo_per_cycle(df_data, df_data_res, price_data, verbose=False):
    if verbose:
        print("\n--- CYCLES ANALYSIS\n")

    individual_cost = []

    for cycle_id in range(max(df_data["CYCLE"])):

        # Isolate the round
        df_cycle_data = df_data[df_data["CYCLE"] == cycle_id]

        # Apply stat on this DF + RES DATA
        indiv_costs = cost_function(df_cycle_data.append(df_data_res), price_data)
        sum_cost = sum([x_pos - x_neg for x_pos, x_neg in indiv_costs.values()])

        # Prints
        if verbose:
            print("\n| CYCLE #{}".format(cycle_id))
            print(" - Community cost: {}".format(sum_cost))
            print(" - Individual costs: {}".format(indiv_costs))

        individual_cost.append(indiv_costs)

    return individual_cost


def analyse_algo_per_round(df_data, df_data_res, price_data, verbose=True):
    def replace_forecast(row, v):
        row["FORECAST_DATA"] = v
        return row

    # Init the DF
    df_round = df_data[df_data["CYCLE"] == 0].copy(deep=True)

    # GO through the algo
    ret = [cost_function(df_round.append(df_data_res), price_data, verbose)]

    for i, data_iter in df_data[df_data["CYCLE"] > 0].iterrows():
        # Update
        mask = df_round["SG_ENTITY_ID"] == data_iter["SG_ENTITY_ID"]
        df_round[mask] = df_round[mask].apply(replace_forecast, args=([data_iter["FORECAST_DATA"]]), axis=1)

        #         print df_round
        # Compute
        ret.append(cost_function(df_round.append(df_data_res), price_data, verbose))

    return ret

# Infinite loop analysis

def same_state(s1, s2):
    for v1, v2 in zip(s1.values, s2.values):
        if not (v1 == v2):
            return False

    return True


def already_seen(s_new, l):
    ret = []
    for s in l:
        if same_state(s_new, s):
            ret.append(s)

    return ret


def list_same_states(df_data):
    identical_cycles = {}

    list_states = []

    for index, data_cycle in df_data.groupby(["CYCLE"]):

        c_list = already_seen(data_cycle['FORECAST_DATA'], list_states)
        if c_list != []:
            for c_id in c_list:
                if c_id in identical_cycles.keys():
                    identical_cycles[c_id].append()
                else:
                    identical_cycles[c_id] = [data_cycle["CYCLE"].values[0]]
        else:
            list_states.append(df_data['FORECAST_DATA'])

    return identical_cycles